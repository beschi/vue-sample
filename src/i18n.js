import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

const messages = {
  en: {
    message: {
      hello: 'hello world',
      helloHtml: 'hello<br/>world',
      welcome: 'Welcome {name}!'
    }
  },
  ja: {
    message: {
      hello: 'こんにちは、世界'
    }
  }
}

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: 'en',
  messages,
  fallbackLocale: 'en'
})

export const setLocale = (lang) => {
  i18n.locale = lang
}

export default i18n

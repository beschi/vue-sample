import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Samples from '@/components/Samples'
import Components from '@/components/Components'
import DynamicRoute from '@/components/Routes/DynamicRoute'
import UserProfile from '@/components/Routes/UserProfile'
import UserSettings from '@/components/Routes/UserSettings'
import VuexExamples from '@/components/VuexExamples'
import Locale from '@/components/Locale'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    }, {
      path: '/samples',
      name: 'Samples',
      component: Samples
    }, {
      path: '/components',
      name: 'Components',
      component: Components
    }, {
      name: 'user',
      path: '/user/:id',
      component: DynamicRoute,
      children: [{
        path: 'profile',
        component: UserProfile
      }, {
        path: 'settings',
        component: UserSettings
      }]
    }, {
      path: '/vuex',
      name: 'VuexExamples',
      component: VuexExamples
    }, {
      path: '/locale',
      name: 'i18n',
      component: Locale
    }
  ]
})

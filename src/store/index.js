import Vue from 'vue'
import Vuex from 'vuex'
import { INCREMENT, DECREMENT } from './mutation-types'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    count: 0,
    todos: [
      { id: 1, text: 'Take printout', done: true },
      { id: 2, text: 'Send courier', done: false }
    ]
  },
  getters: {
    doneTodos: state => state.todos.filter(todo => todo.done),
    doneTodosCount: (state, getters) => getters.doneTodos.length
  },
  mutations: {
    [INCREMENT] (state, payload) {
      if (payload && payload.amount) {
        state.count += payload.amount
      } else {
        state.count++
      }
    },
    [DECREMENT] (state) {
      state.count--
    }
  },
  actions: {
    [INCREMENT] ({ commit }) {
      commit(INCREMENT)
    },
    [DECREMENT] ({ commit }) {
      commit(DECREMENT)
    },
    // we can do like store.dispatch('asyncAction').then()
    asyncAction ({ commit }) {
      return new Promise((resolve, reject) => {
        // or some ajax call
        setTimeout(() => {
          commit('someMutation')
          resolve()
        }, 1000)
      })
    }
  }
})

export default store
